const $config = require('config');

const actionFlow = require('action-flow')({
  db: $config.get('mongo.db')
});

module.exports = actionFlow;