const $mongo = require('../libs/mongo');
const $TelegraphAPI = require('./telegraph-api');
const $Promise = require('bluebird');
const $escapeRegExp = require('escape-string-regexp');

class Posts {
  static get postsDB () {
    return $mongo.collection('posts');
  }

  static async list (telegraphToken, data) {
    if (data.query) {
      data.query = $escapeRegExp(data.query);
      return Posts.search(telegraphToken, data);
    }

    const offset = data.offset || 0;
    const limit = 15;

    const list = await $TelegraphAPI.query('getPageList', {
      access_token: telegraphToken,
      offset, limit
    })
      .then((data) => {
        if (!data.ok) {
          throw data;
        }

        return data.result;
      });

    return Posts.aggregate(list.pages);
  }

  static async search (telegraphToken, data) {
    let offset = data.offset || 0;

    const query = new RegExp(data.query, 'i');
    const limit = 100;
    const limitReturn = 15;
    const resultList = [];

    await new $Promise((resolve, reject) => {
      (async function get () {
        const list = await $TelegraphAPI.query('getPageList', {
          access_token: telegraphToken,
          offset, limit
        })
          .then((data) => {
            if (!data.ok) {
              throw data;
            }

            return data.result;
          });

        const pages = list.pages;

        for (const page of pages) {
          if (page.title.match(query)) {
            resultList.push(page);
          }
        }

        if (resultList.length >= limitReturn || pages.length < limitReturn) {
          resolve();
        } else {
          offset += limit;
          setImmediate(get);
        }
      })().catch(reject);
    });

    return Posts.aggregate(resultList);
  }

  static async aggregate (list) {
    const paths = [];
    const result = [];

    for (const post of list) {
      paths.push(post.path);
    }

    const existsPosts = await Posts.postsDB.find({
      path: {$in: paths}
    }, {
      sort: {
        createdDate: -1
      },
      project: {
        path: 1,
        shortId: 1,
        preview: 1,
        votes: 1,
        createdDate: 1
      }
    });

    const existsPostsPaths = [];

    for (const post of existsPosts) {
      existsPostsPaths.push(post.path);
      result.push({
        path: post.path,
        short_id: post.shortId,
        date: post.createdDate,
        preview: post.preview,
        votes: post.votes
      });
    }

    for (const post of list) {
      if (existsPostsPaths.includes(post.path)) {
        continue;
      }

      result.push({
        path: post.path,
        preview: {
          path: post.path,
          url: post.url,
          title: post.title,
          description: post.description,
          author_name: post.author_name,
          views: post.views
        }
      });
    }

    return result.sort((a, b) => {
      if (a.short_id && !b.short_id) {
        return 1;
      } else if (!a.short_id && b.short_id) {
        return -1;
      }

      return 0;
    });
  }
}

module.exports = Posts;
