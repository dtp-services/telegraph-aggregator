const DTP_API_PATH = '../../dtp-api/';
const PORT = require('../../dtp-ecosystem/reference/port');
const ENV = process.env.NODE_ENV;

module.exports = {
  server: {
    port: PORT['telegraph-aggregator'][ENV],
    middlewares: require('./system/middlewares'),
    handlers: require('./system/handlers')
  },
  mongo: {
    db: 'DTP-Database',
    models: require(DTP_API_PATH + 'config/system/mongo-models'),
    log: process.env.LOG_LEVEL || 'info'
  },
  services: {
    dtp: {
      url: ''
    }
  }
};